# AppTest #

Super simple experiment with creating a cocoa application using only Swift Package Manager, no Xcode.

Build and run with 
```
#!bash

swift build && ./.build/debug/AppTest
```

## Notes ##

* The application created is a single file executable, not a `.app` bundle
* There's no `info.plist` file although you could compile one into the executable with a linker flag
* You could set the Dock icon with `NSDockTile`


## Sources and Further Reading ##

* http://czak.pl/2015/09/23/single-file-cocoa-app-with-swift.html
* https://www.cocoawithlove.com/2010/09/minimalist-cocoa-programming.html
* https://www.cocoawithlove.com/2009/01/demystifying-nsapplication-by.html
* https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Attributes.html
* https://github.com/eonil/CocoaProgrammaticHowtoCollection