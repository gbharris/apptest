import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
	
	lazy var window: NSWindow = {
		return NSWindow(contentRect: NSMakeRect(0, 0, 320, 200),
		                styleMask: [.titled,.closable,.resizable,.miniaturizable],
		                backing: .buffered,
		                defer: true)
	}()
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		NSLog("\(#function)")
		window.makeKeyAndOrderFront(nil)
		window.center()
		NSApp.activate(ignoringOtherApps: true)

	}
	
	func applicationWillTerminate(_ aNotification: Notification) {
		NSLog("\(#function)")
	}
	
}
