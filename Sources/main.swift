import Cocoa

NSApplication.shared()
NSApp.setActivationPolicy(.regular)

let delegate = AppDelegate()
NSApp.delegate = delegate

let menubar = NSMenu()
let appMenuItem = NSMenuItem()
menubar.addItem(appMenuItem)
NSApp.mainMenu = menubar

let appMenu = NSMenu()
let appName = ProcessInfo.processInfo.processName
let quitMenuItem = NSMenuItem(title: "Quit \(appName)", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q")
appMenu.addItem(quitMenuItem)
appMenuItem.submenu = appMenu

NSApp.run()
